/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID 
    that is passed to it from the given data in lists.json.
     Then pass control back to the code that called it by using a callback function.
*/
const fs = require("fs");
function callback(id, listsPath, cb) {
	setTimeout(() => {
		if (typeof cb === "function") {
			fs.readFile(listsPath, "utf-8", (err, lists) => {
				if (err) {
					console.error(listsPath + " does not exist. Check the path again");
					return;
				} else {
					try {
						let listsData = JSON.parse(lists);
						if (listsData[id] !== undefined) {
							cb(null, listsData[id]);
						} else {
							cb(new Error("Data not found for " + id));
						}
					} catch (err) {
						cb(new Error("Wrong data Passed"));
					}
				}
			});
		} else {
			console.error("CB is not a function");
		}
	}, 2 * 1000);
}

module.exports = callback;
