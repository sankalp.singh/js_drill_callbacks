/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID
	 that is passed from the given list of boards in boards.json 
	 and then pass control back to the code that called it by using a callback function.
*/
const fs = require("fs");
function callback(info, boardsPath, cb) {
	setTimeout(() => {
		if (typeof cb === "function") {
			fs.readFile(boardsPath, "utf-8", (err, boards) => {
				if (err) {
					cb(new Error(boardsPath + " does not exist. Check the path again"));
					return;
				} else {
					try {
						let boardsData = JSON.parse(boards);
						let result = boardsData.filter(
							(items) => items.id === info || items.name === info
						);
						if (result.length === 0) {
							cb(new Error("Data not found for : " + info));
						} else {
							cb(null, result);
						}
					} catch (error) {
						cb(new Error("Wrong Data is present"));
						return;
					}
				}
			});
		} else {
			console.error("CB is not a function");
		}
	}, 2 * 1000);
}

module.exports = callback;
