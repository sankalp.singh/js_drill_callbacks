/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID 
    that is passed to it from the given data in cards.json. 
    Then pass control back to the code that called it by using a callback function.
*/
const fs = require("fs");
function callback(id, cardsPath, cb) {
	setTimeout(() => {
		if (typeof cb === "function") {
			fs.readFile(cardsPath, "utf-8", (err, cards) => {
				if (err) {
					console.error(cardsPath + " does not exist. Check the path again");
				} else {
					try {
						let cardsData = JSON.parse(cards);
						if (cardsData[id] !== undefined) {
							cb(null, cardsData[id]);
							return;
						} else {
							cb(new Error("Data not found for " + id));
							return;
						}
					} catch (err) {
						cb(new Error("Wrong Data Passed!"));
					}
				}
			});
		} else {
			console.error("CB is not a function");
		}
	}, 2 * 1000);
}
module.exports = callback;
