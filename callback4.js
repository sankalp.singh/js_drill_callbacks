/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. 
    You do not need to pass control back to the code that called it.    
    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/
const fs = require("fs");
const path = require("path");
const boardPath = path.join(__dirname, "../boards.json");
const listsPath = path.join(__dirname, "../lists.json");
const cardsPath = path.join(__dirname, "../cards.json");
function callback(callback1, callback2, callback3, cb) {
	setTimeout(() => {
		if (typeof cb === "function") {
			try {
				callback1("Thanos", boardPath, function cb1(err, boards) {
					if (err) {
						cb(err);
					} else {
						cb(null, boards);
						let boardsData = boards.find((items) => items.name === "Thanos");
						callback2(boardsData.id, listsPath, function cb2(err, lists) {
							if (err) {
								cb(err);
							} else {
								cb(null, lists);
								for (let items of lists) {
									if (items.name === "Mind") {
										callback3(items.id, cardsPath, function cb3(err, cards) {
											if (err) {
												cb(err);
											} else {
												cb(null, cards);
											}
										});
									}
								}
							}
						});
					}
				});
			} catch (err) {
				console.error("Path is not defined correctly");
			}
		} else {
			console.error("CB is not passed as function");
		}
	}, 2 * 1000);
}
module.exports = callback;
