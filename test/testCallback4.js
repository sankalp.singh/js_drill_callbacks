const fs = require("fs");
const callback = require("../callback4");
const callback1 = require("../callback1");
const callback2 = require("../callback2");
const callback3 = require("../callback3");
// const callback = require("../callback4");
try {
	callback(callback1, callback2, callback3, function cb(err, data) {
		if (err) {
			console.error(err);
		} else {
			console.log(data);
		}
	});
} catch (err) {
	console.log("Wrong data is passed!");
}
// callback();
