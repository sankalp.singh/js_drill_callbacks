const fs = require("fs");
const callback = require("../callback1");
const path = require("path");

const boardsPath = path.join(__dirname, "../../boards.json");

try {
	callback("abc122dc", boardsPath, function cb(err, result) {
		if (err) {
			console.error(err);
		} else {
			console.log(result);
		}
	});
} catch (err) {
	console.error("Path is not defined correctly");
}
