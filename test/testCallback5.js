/* 
	Problem 5: Write a function that will use the previously written functions to get the following information.
     You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/
const fs = require("fs");
const callback = require("../callback5");
const callback1 = require("../callback1");
const callback2 = require("../callback2");
const callback3 = require("../callback3");
// const callback = require("../callback4");
try {
	callback(callback1, callback2, callback3, function cb(err, data) {
		if (err) {
			console.log(err);
		} else {
			console.log(data);
		}
	});
} catch (err) {
	console.error("Wrong data is passed");
}
