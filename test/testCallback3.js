const callback = require("../callback3");
const path = require("path");
const cardsPath = path.join(__dirname, "../../cardas.json");

callback("jwkh245", cardsPath, function cb(err, result) {
	if (err) {
		console.error(err.message);
	} else {
		console.log(result);
	}
});
