const callback = require("../callback2");
const path = require("path");
const listsPath = path.join(__dirname, "../../lists.json");

try {
	callback("mcu453ed", listsPath, function cb(err, result) {
		if (err) {
			console.error(err);
		} else {
			console.log(result);
		}
	});
} catch (err) {
	console.error("path is not defined correctly! ");
}
