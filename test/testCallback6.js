const fs = require("fs");
const callback = require("../callback6");
const callback1 = require("../callback1");
const callback2 = require("../callback2");
const callback3 = require("../callback3");
// const callback = require("../callback4");
callback(callback1, callback2, callback3, function cb(err, data) {
	if (err) {
		console.error(err);
	} else {
		console.log(data);
	}
});
